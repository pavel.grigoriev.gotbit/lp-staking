import { ethers } from 'hardhat'
import { expect, use } from 'chai'
import { loadFixture } from 'ethereum-waffle'

import {
  Minting__factory,
  Token__factory,
  Minting,
  Token,
  Factory__factory,
  Factory,
} from '@/typechain'
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'
import { BigNumber, BigNumberish, Contract, ContractFactory } from 'ethers/lib/ethers'
import { ERRORS } from './Errors'
import { randomAddress, sleep, useSigner } from './helper'
import { epsEqual } from './helper'

import * as UniswapV2FactoryData from '@uniswap/v2-core/build/UniswapV2Factory.json'
import * as UniswapV2PairData from '@uniswap/v2-core/build/UniswapV2Pair.json'
import * as UniswapV2RouterData from '@uniswap/v2-periphery/build/UniswapV2Router02.json'
import * as WETH9Data from '@uniswap/v2-periphery/build/WETH9.json'

describe('Minting contract', () => {
  let mintingFactory: Minting__factory
  let minting: Minting
  let baseImpl: Minting
  let factoryFactory: Factory__factory
  let factory: Factory

  let uniswapV2Router: Contract
  let uniswapV2Factory: Contract
  const ADDRESS_ZERO = ethers.constants.AddressZero

  let deployer: SignerWithAddress
  let user0: SignerWithAddress
  let user1: SignerWithAddress

  let token: Token
  let usdt: Token

  let ADMIN_ROLE: string
  let DEFAULT_ADMIN_ROLE: string

  const ONE = ethers.constants.WeiPerEther
  const deadline = BigNumber.from(10).pow(20)

  let pair: Contract

  const DAY = 86400
  const MONTH = 30 * DAY
  const YEAR = 365 * DAY

  let DENOMINATOR: BigNumber

  const deployFixture = async () => {
    ;[deployer, user0, user1] = await ethers.getSigners()

    mintingFactory = await ethers.getContractFactory<Minting__factory>('Minting')
    factoryFactory = await ethers.getContractFactory<Factory__factory>('Factory')

    baseImpl = await mintingFactory.deploy()
    await baseImpl.deployed()

    const _usdt = await (
      await ethers.getContractFactory<Token__factory>('Token')
    ).deploy('BEP20USDT', 'USDT')

    const _factory = await factoryFactory.deploy(baseImpl.address)
    expect(_factory.address).to.not.eq(ADDRESS_ZERO)
    expect(baseImpl.address).to.not.eq(ADDRESS_ZERO)
    expect(_usdt.address).to.not.eq(ADDRESS_ZERO)

    const uniswapRouterFactory = new ContractFactory(
      UniswapV2RouterData.abi,
      UniswapV2RouterData.bytecode,
      deployer
    )
    const uniswapFactoryFactory = new ContractFactory(
      UniswapV2FactoryData.abi,
      UniswapV2FactoryData.bytecode,
      deployer
    )
    const wethFactory = new ContractFactory(WETH9Data.abi, WETH9Data.bytecode, deployer)

    const weth = await wethFactory.deploy()

    const _uniswapV2Factory = await uniswapFactoryFactory.deploy(randomAddress())
    const _uniswapV2Router = await uniswapRouterFactory.deploy(
      _uniswapV2Factory.address,
      weth.address
    )

    expect(_uniswapV2Router.address).to.not.eq(ADDRESS_ZERO)
    expect(_uniswapV2Factory.address).to.not.eq(ADDRESS_ZERO)

    const _token = await (
      await ethers.getContractFactory<Token__factory>('Token')
    ).deploy('Test', 'TST')

    expect(_token.address).to.not.eq(ADDRESS_ZERO)

    ADMIN_ROLE = await _factory.ADMIN_ROLE()
    DEFAULT_ADMIN_ROLE = await _factory.DEFAULT_ADMIN_ROLE()

    await _factory.grantRole(ADMIN_ROLE, deployer.address)

    await _uniswapV2Factory.createPair(_token.address, _usdt.address)

    const pairAddress = await _uniswapV2Factory.getPair(_token.address, _usdt.address)
    expect(pairAddress).to.not.eq(ADDRESS_ZERO)

    const _pair = await ethers.getContractAt(UniswapV2PairData.abi, pairAddress)

    return {
      deployer,
      user0,
      user1,
      _uniswapV2Router,
      _uniswapV2Factory,
      _pair,
      _factory,
      _token,
      _usdt,
    }
  }

  const addLiquidity = async (
    signer: SignerWithAddress,
    amount0: BigNumberish,
    amount1: BigNumberish,
    tokenA: Token,
    tokenB: Token,
    uniRouter?: Contract
  ) => {
    await tokenA.mint(signer.address, amount0)
    await tokenB.mint(signer.address, amount1)

    uniRouter = uniRouter ?? uniswapV2Router

    await tokenA.connect(signer).approve(uniRouter.address, amount0)
    await tokenB.connect(signer).approve(uniRouter.address, amount1)

    await uniRouter
      .connect(signer)
      .addLiquidity(
        tokenA.address,
        tokenB.address,
        amount0,
        amount1,
        0,
        0,
        signer.address,
        deadline
      )

    DENOMINATOR = await baseImpl.DENOMINATOR()
  }

  const removeLiquidity = async (
    signer: SignerWithAddress,
    amountLP: BigNumberish,
    tokenA: Token,
    tokenB: Token,
    tokenLP: Contract,
    uniRouter?: Contract
  ) => {
    uniRouter = uniRouter ?? uniswapV2Router

    await tokenLP.connect(signer).approve(uniRouter.address, amountLP)

    // address tokenA,
    // address tokenB,
    // uint liquidity,
    // uint amountAMin,
    // uint amountBMin,
    // address to,
    // uint deadline

    await uniRouter
      .connect(signer)
      .removeLiquidity(
        tokenA.address,
        tokenB.address,
        amountLP,
        0,
        0,
        signer.address,
        deadline
      )

    DENOMINATOR = await baseImpl.DENOMINATOR()
  }

  beforeEach(async () => {
    const state = await loadFixture(deployFixture)
    deployer = state.deployer
    user0 = state.user0
    user1 = state.user1
    uniswapV2Router = state._uniswapV2Router
    uniswapV2Factory = state._uniswapV2Factory
    pair = state._pair
    factory = state._factory
    token = state._token
    usdt = state._usdt

    await addLiquidity(deployer, ONE.mul(1000), ONE.mul(10000), token, usdt)
  })

  it('stake + withdraw flow', async () => {
    await addLiquidity(user0, ONE, ONE.mul(10), token, usdt)
    await factory
      .connect(deployer)
      .createPool(
        uniswapV2Router.address,
        token.address,
        usdt.address,
        DENOMINATOR.mul(5).div(2),
        DENOMINATOR.mul(2)
      )

    let reserves = await pair.getReserves()
    expect(reserves[0]).to.eq(ONE.mul(1000 + 1))
    expect(reserves[1]).to.eq(ONE.mul(10000 + 10))

    const userLP = await pair.balanceOf(user0.address)
    expect(userLP).to.be.gt(0)

    const mintingAddress = (await factory.getPoolsSlice(0, 1))[0]
    minting = await ethers.getContractAt<Minting>('Minting', mintingAddress)

    const maxReserve = ONE.mul(ONE.mul(10))

    await token.mint(minting.address, maxReserve)
    await usdt.mint(minting.address, maxReserve)

    await pair.connect(user0).approve(minting.address, userLP)
    await minting.connect(user0).stake(userLP, DAY * 60)

    await sleep(60 * DAY)

    await minting.connect(user0).withdraw(1)

    reserves = await pair.getReserves()
    expect(epsEqual(reserves[0], ONE.mul(1000))).to.be.true
    expect(epsEqual(reserves[1], ONE.mul(10000))).to.be.true

    const aprNumerator = 10000
    expect(aprNumerator).to.eq(await minting.getAPRForDuration(60 * DAY))

    // price const => rewards = 10 % of added liquidity

    const tokenBalance = await token.balanceOf(user0.address)
    const usdtBalance = await usdt.balanceOf(user0.address)

    expect(
      epsEqual(
        tokenBalance,
        ONE.mul(DENOMINATOR.add(aprNumerator))
          .mul(60 * DAY)
          .div(DENOMINATOR.mul(YEAR))
      )
    )

    expect(
      epsEqual(
        usdtBalance,
        ONE.mul(10)
          .mul(DENOMINATOR.add(aprNumerator))
          .mul(60 * DAY)
          .div(DENOMINATOR.mul(YEAR))
      )
    )

    await addLiquidity(user1, ONE, ONE.mul(10), token, usdt)
    const user1LP = await pair.balanceOf(user1.address)
    expect(user1LP).to.be.gt(0)

    reserves = await pair.getReserves()
    expect(epsEqual(reserves[0], ONE.mul(1000 + 1))).to.be.true
    expect(epsEqual(reserves[1], ONE.mul(10000 + 10))).to.be.true

    await pair.connect(user1).approve(minting.address, user1LP)
    await minting.connect(user1).stake(user1LP, DAY * 365)

    // price changes

    await token.mint(deployer.address, ONE.mul(5))
    await token.approve(uniswapV2Router.address, ONE.mul(5))

    await uniswapV2Router
      .connect(deployer)
      .swapExactTokensForTokens(
        ONE.mul(5),
        0,
        [token.address, usdt.address],
        deployer.address,
        deadline
      )

    reserves = await pair.getReserves()
    expect(epsEqual(reserves[0], ONE.mul(1000 + 1 + 5))).to.be.true

    const expectedReserve = ONE.mul(1001).mul(10010).div(1006)
    expect(epsEqual(reserves[1], expectedReserve)).to.be.true

    const period = DAY * 20
    await sleep(period)

    await minting.connect(user1).withdraw(2)

    const aprNumeratorNew = 7500

    const tokenBalance1 = await token.balanceOf(user1.address)
    const usdtBalance1 = await usdt.balanceOf(user1.address)

    const userReserve0 = ONE.mul(1001).div(1000)
    const userReserve1 = ONE.mul(9960).div(10000)

    expect(
      epsEqual(
        tokenBalance1,
        userReserve0
          .mul(DENOMINATOR.add(aprNumeratorNew))
          .mul(period)
          .div(DENOMINATOR.mul(YEAR))
      )
    )

    expect(
      epsEqual(
        usdtBalance1,
        userReserve1
          .mul(DENOMINATOR.add(aprNumeratorNew))
          .mul(period)
          .div(DENOMINATOR.mul(YEAR))
      )
    )

    await removeLiquidity(
      deployer,
      await pair.balanceOf(deployer.address),
      token,
      usdt,
      pair
    )
  })
})
