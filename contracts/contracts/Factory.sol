//SPDX-License-Identifier: MIT
pragma solidity =0.8.18;

import '@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol';
import '@openzeppelin/contracts/utils/structs/EnumerableSet.sol';
import '@openzeppelin/contracts/access/AccessControlEnumerable.sol';
import '@openzeppelin/contracts/proxy/Clones.sol';

import './Minting.sol';

contract Factory is AccessControlEnumerable {
    using EnumerableSet for EnumerableSet.AddressSet;

    bytes32 public constant ADMIN_ROLE = keccak256(abi.encodePacked('ADMIN_ROLE'));

    address[] public pools;

    address public immutable baseImplementation;

    modifier onlySupportedPool(uint256 poolIndex) {
        require(poolIndex < pools.length, 'Pool not supported');
        _;
    }

    modifier onlyFundsManager(uint256 poolIndex) {
        address pool = pools[poolIndex];
        require(hasRole(roleForPool(pool), msg.sender), 'Not funds manager');
        _;
    }

    /// @notice Creates a new contract
    /// @param impl_ - Minting pool base implementation
    constructor(address impl_) {
        require(impl_ != address(0), 'Impl 0x0');

        baseImplementation = impl_;

        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    /// @notice Creates a role for each pool contract
    /// @param poolAddr - Minting pool address
    /// @return role - bytes32 role hash("FUNDS_MANAGER_ROLE" + poolAddr)
    function roleForPool(address poolAddr) public pure returns (bytes32) {
        string memory role = 'FUNDS_MANAGER_ROLE';
        string memory roleStr = string.concat(role, string(abi.encodePacked(poolAddr)));
        return keccak256(abi.encode(roleStr));
    }

    /// @notice Creates a new pool
    /// @param router_ - Uniswap V2 router
    /// @param tokenA_ - token A address
    function createPool(
        address router_,
        address tokenA_,
        address tokenB_,
        uint256 factorA_,
        uint256 factorB_
    ) external onlyRole(ADMIN_ROLE) {
        address pool = Clones.clone(baseImplementation);

        Minting(payable(pool)).initialize(
            router_,
            tokenA_,
            tokenB_,
            factorA_,
            factorB_,
            address(this)
        );

        pools.push(pool);
    }

    /// @notice Can withdraw extra funds from the pool contract (can be called by the owner only)
    /// @param pool - staking pool index in pools array
    /// @param recepient - recepient address
    /// @param amountA - amount of token A
    /// @param amountUSDT - amount of token USDT
    function withdrawAvailableRewards(
        uint256 pool,
        address recepient,
        uint256 amountA,
        uint256 amountUSDT
    ) external onlySupportedPool(pool) onlyFundsManager(pool) {
        Minting(pools[pool]).emergencyWithdrawRewards(amountA, amountUSDT, recepient);
    }

    /// @notice Locks a specific staking pool
    /// @param pool - staking pool index in pools array
    function lockPool(
        uint256 pool
    ) external onlyRole(ADMIN_ROLE) onlySupportedPool(pool) {
        require(!Minting(pools[pool]).paused(), 'Paused');
        Minting(pools[pool]).setPaused(true);
    }

    /// @notice Unlocks a specific staking pool
    /// @param pool - staking pool index in pools array
    function unlockPool(
        uint256 pool
    ) external onlyRole(ADMIN_ROLE) onlySupportedPool(pool) {
        require(Minting(pools[pool]).paused(), 'Unpaused');
        Minting(pools[pool]).setPaused(false);
    }

    /// @notice Allows to get a slice of pools array
    /// @param offset Starting index in user ids array
    /// @param length return array length
    /// @return Array-slice of pools
    function getPoolsSlice(
        uint256 offset,
        uint256 length
    ) external view returns (address[] memory) {
        address[] memory res = new address[](length);
        for (uint256 i; i < length; ) {
            res[i] = pools[i + offset];
            unchecked {
                ++i;
            }
        }

        return res;
    }

    /// @notice Allows to get a length of pools array
    /// @return Length of user pools array
    function getPoolsLength() external view returns (uint256) {
        return pools.length;
    }
}
