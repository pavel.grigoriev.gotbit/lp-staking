//SPDX-License-Identifier: MIT
pragma solidity =0.8.18;

import '@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router01.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2ERC20.sol';
import '@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol';

import '@openzeppelin/contracts/access/Ownable.sol';
import '@openzeppelin/contracts/security/Pausable.sol';
import '@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol';
import '@openzeppelin/contracts/utils/structs/EnumerableSet.sol';

contract Minting is Ownable, Pausable {
    using SafeERC20 for IERC20;
    using EnumerableSet for EnumerableSet.UintSet;
    using EnumerableSet for EnumerableSet.AddressSet;

    struct Reserves {
        uint256 reserveA;
        uint256 reserveB;
    }

    // TYPES

    struct Stake {
        address owner;
        uint256 amount;
        Reserves reserves;
        uint80 duration;
        uint80 timestamp;
        uint80 unstakedAtBlockTimestamp;
    }

    // STATE VARIABLES

    uint256 public constant DENOMINATOR = 100_000;
    uint256 public constant MIN_REWARDS_DURATION = 365 days; // 1 years
    uint256 public constant MIN_STAKE_AMOUNT = 1000;
    uint256 public constant MAX_STAKE_AMOUNT = 10 ** 70;
    uint256 public constant MIN_STAKE_PERIOD = 1 days;
    uint256 public constant YEAR = 365 days;
    uint256 public constant MAX_STAKE_PERIOD = YEAR;
    uint256 public constant APR_5 = 5_000;
    uint256 public constant APR_7_5 = 7_500;
    uint256 public constant APR_10 = 10_000;
    uint256 public constant APR_12_5 = 12_500;
    uint256 public constant APR_15 = 15_000;

    // IMMUTABLE

    address public router;
    address public factory;
    address public pair;

    IERC20 public rewardTokenA;
    IERC20 public rewardTokenB;

    uint256 public tokenAFactor;
    uint256 public tokenBFactor;

    // MUTABLE

    uint256 public maxPotentialDebtA;
    uint256 public maxPotentialDebtB;

    mapping(uint256 => Stake) public stakes;
    mapping(address => uint256[]) public userInactiveStakes;
    mapping(address => EnumerableSet.UintSet) private idsByUser;

    mapping(address => uint256) public userBalances;

    uint256 public globalId;

    bool public initialized;

    // number of currenlty active stakes
    uint128 public numOfActiveStakes;
    // activa steke holder
    EnumerableSet.AddressSet private holders;

    event RewardAdded(uint256 reward);
    event Staked(address indexed user, uint256 indexed id, uint256 amount);
    event Withdrawn(address user, uint256 indexed id, uint256 amount);
    event RewardPaid(address user, uint256 indexed id, uint256 reward);
    event RewardsDurationUpdated(uint256 newDuration);

    modifier whenInitialized() {
        require(initialized, 'Not init');
        _;
    }

    /// @notice Initialized minting pool
    /// @param router_ - Uniswap V2 router
    /// @param tokenA_ - token A address
    /// @param tokenB_ - token B address
    /// @param owner_ - owner address
    function initialize(
        address router_,
        address tokenA_,
        address tokenB_,
        uint256 tokenAFactor_,
        uint256 tokenBFactor_,
        address owner_
    ) external {
        require(!initialized, 'Already init');
        require(address(router_) != address(0), 'Invalid address');
        require(address(tokenA_) != address(0), 'Invalid address');
        require(address(tokenB_) != address(0), 'Invalid address');
        require(owner_ != address(0), 'Invalid address');
        require(tokenAFactor_ > DENOMINATOR, 'Factor A too low');
        require(tokenBFactor_ > DENOMINATOR, 'Factor B too low');

        initialized = true;

        tokenAFactor = tokenAFactor_;
        tokenBFactor = tokenBFactor_;

        router = router_;
        factory = IUniswapV2Router01(router).factory();
        pair = IUniswapV2Factory(factory).getPair(tokenA_, tokenB_);
        require(address(pair) != address(0), 'Pair not found');

        rewardTokenA = IERC20(tokenA_);
        rewardTokenB = IERC20(tokenB_);

        _transferOwnership(owner_);
    }

    /// @dev Allows user to stake UniV2 LP tokens
    function stake(
        uint256 amount,
        uint256 stakingPeriod
    ) external whenNotPaused whenInitialized {
        require(
            amount >= MIN_STAKE_AMOUNT && amount <= MAX_STAKE_AMOUNT,
            'Amount out of bounds'
        );
        require(
            stakingPeriod >= MIN_STAKE_PERIOD && stakingPeriod <= MAX_STAKE_PERIOD,
            'Period out of bounds'
        );

        (uint256 reserveA, uint256 reserveB) = _calculateUserReserves(amount);

        Reserves memory res = Reserves({reserveA: reserveA, reserveB: reserveB});

        (uint256 maxReserveA, uint256 maxReserveB) = _calculateMaxPotentialReserves(res);

        // get full profit percentage
        uint256 maxAPR = getAPRForDuration(stakingPeriod);

        // check debt
        maxPotentialDebtA +=
            (maxReserveA * maxAPR * stakingPeriod) /
            (DENOMINATOR * YEAR);
        maxPotentialDebtB +=
            (maxReserveB * maxAPR * stakingPeriod) /
            (DENOMINATOR * YEAR);

        require(
            maxPotentialDebtA <= rewardTokenA.balanceOf(address(this)),
            'Debt token A exceeded'
        );
        require(
            maxPotentialDebtB <= rewardTokenB.balanceOf(address(this)),
            'Debt token B exceeded'
        );

        uint256 id = ++globalId;
        stakes[id] = Stake({
            owner: msg.sender,
            amount: amount,
            reserves: res,
            duration: uint80(stakingPeriod),
            timestamp: uint80(block.timestamp),
            unstakedAtBlockTimestamp: 0
        });

        idsByUser[msg.sender].add(id);

        if (userBalances[msg.sender] == 0) holders.add(msg.sender);

        userBalances[msg.sender] += amount;
        ++numOfActiveStakes;

        // get LPs
        IERC20(pair).safeTransferFrom(msg.sender, address(this), amount);
    }

    /// @dev Allows user to withdraw staked NFTs + claim earned rewards - penalties
    /// @param id Stake id
    function withdraw(uint256 id) external whenInitialized {
        Stake memory _stake = stakes[id];
        require(_stake.unstakedAtBlockTimestamp == 0, 'Already unstaked');
        require(_stake.owner == msg.sender, 'Can`t be called not by stake owner');

        (uint256 maxReserveA, uint256 maxReserveB) = _calculateMaxPotentialReserves(
            _stake.reserves
        );

        // get full profit percentage
        uint256 maxAPR = getAPRForDuration(_stake.duration);

        uint256 potentialBalanceA = (maxReserveA * maxAPR * _stake.duration) /
            (DENOMINATOR * YEAR);

        uint256 potentialBalanceB = (maxReserveB * maxAPR * _stake.duration) /
            (DENOMINATOR * YEAR);

        // reduce debt
        maxPotentialDebtA = _safeDecrease(maxPotentialDebtA, potentialBalanceA);
        maxPotentialDebtB = _safeDecrease(maxPotentialDebtB, potentialBalanceB);

        idsByUser[msg.sender].remove(id);

        userBalances[msg.sender] -= _stake.amount;
        if (userBalances[msg.sender] == 0) holders.remove(msg.sender);
        --numOfActiveStakes;

        IERC20(pair).approve(router, _stake.amount);
        (uint256 actualReserveA, uint256 actualReserveB) = IUniswapV2Router01(router)
            .removeLiquidity(
                address(rewardTokenA),
                address(rewardTokenB),
                _stake.amount,
                0,
                0,
                address(this),
                block.timestamp + 1
            );

        // calculate rewards
        uint256 actualHoldTime = _getActualHoldTime(
            (block.timestamp - _stake.timestamp),
            _stake.duration
        );

        uint256 apr = getAPRForDuration(actualHoldTime);

        // user reserve = min (max potential reserve, actual reserve)
        uint256 actualRewardA = (_min(actualReserveA, maxReserveA) *
            (DENOMINATOR + apr) *
            actualHoldTime) / (DENOMINATOR * YEAR);
        // actualRewardB <= maxReserve * apr
        uint256 actualRewardB = (_min(actualReserveB, maxReserveB) *
            (DENOMINATOR + apr) *
            actualHoldTime) / (DENOMINATOR * YEAR);

        IERC20(rewardTokenA).safeTransfer(_stake.owner, actualRewardA);
        IERC20(rewardTokenB).safeTransfer(_stake.owner, actualRewardB);
    }

    /// @notice Pulls out the amount of unused rewards
    /// @param rewardA - amouunt of token A
    /// @param rewardB - amouunt of token B
    /// @param recepient - tokens receiver
    function emergencyWithdrawRewards(
        uint256 rewardA,
        uint256 rewardB,
        address recepient
    ) external onlyOwner whenInitialized {
        require(rewardA != 0 && rewardB != 0, 'Zero amount');
        require(recepient != address(0), 'Zero recepient');

        (uint256 rewardAAvailable, uint256 rewardBAvailable) = getRewardsAvailable();
        require(
            rewardA <= rewardAAvailable && rewardB <= rewardBAvailable,
            'Amount too high'
        );

        IERC20(rewardTokenA).safeTransfer(recepient, rewardA);
        IERC20(rewardTokenB).safeTransfer(recepient, rewardB);
    }

    /// @notice Returns amount of unused rewards available for pulling out by funds manager
    /// @return rewardA - reward in token A, rewardB - reward in token B
    function getRewardsAvailable()
        public
        view
        returns (uint256 rewardA, uint256 rewardB)
    {
        rewardA = IERC20(rewardTokenA).balanceOf(address(this)) - maxPotentialDebtA;
        rewardB = IERC20(rewardTokenB).balanceOf(address(this)) - maxPotentialDebtB;
    }

    /// @dev Returns the approximate APR for a specific stake position not including penalties, when TVL == 0 => returns 0
    /// @param duration - stake duration in seconds
    /// @return apr value multiplied by 10**18
    function getAPRForDuration(uint256 duration) public pure returns (uint256 apr) {
        if (duration <= 7 days) {
            // week
            return APR_5;
        } else if (duration <= 30 days) {
            // month
            return APR_7_5;
        } else if (duration <= 3 * 30 days) {
            // 3 month
            return APR_10;
        } else if (duration <= 6 * 30 days) {
            // half a year
            return APR_12_5;
        } else {
            // year
            return APR_15;
        }
    }

    /// @dev Returns min of hold time and duration
    /// @param holdTime - stake hold time in seconds
    /// @param duration - stake duration in seconds
    /// @return Min of hold time and duration
    function _getActualHoldTime(
        uint256 holdTime,
        uint256 duration
    ) private pure returns (uint256) {
        return (holdTime > duration) ? duration : holdTime;
    }

    /// @dev Calculates max user reserves in each token in case if there is only one wei of any asset in the pool
    /// @param reserves_ - user initial reserves
    /// @return maxReserveA - Max potential reserve in tokenA, maxReserveB - Max potential reserve in tokenB
    function _calculateMaxPotentialReserves(
        Reserves memory reserves_
    ) private view returns (uint256 maxReserveA, uint256 maxReserveB) {
        // maxReserveA = reserveA * tokenA_factor / DENOMINATOR
        maxReserveA = (reserves_.reserveA * tokenAFactor) / DENOMINATOR;
        maxReserveB = (reserves_.reserveB * tokenBFactor) / DENOMINATOR;
    }

    /// @dev Calculates user reserves in each token
    /// @param amountLP - user LP token amount
    /// @return userReserveA - user reserve in token A, userReserveB - user reserve in token B
    function _calculateUserReserves(
        uint256 amountLP
    ) private view returns (uint256 userReserveA, uint256 userReserveB) {
        uint256 totalLP = IUniswapV2ERC20(pair).totalSupply();
        if (totalLP == 0) return (0, 0);

        (uint256 reserveA, uint256 reserveB, ) = IUniswapV2Pair(pair).getReserves();

        // userLP = ((userLP / totalLP) * reserveA, (userLP / totalLP) * reserveB)
        userReserveA = (amountLP * reserveA) / totalLP;
        userReserveB = (amountLP * reserveB) / totalLP;
    }

    /// @dev Calculates user LP amount in each token after add liquiddity
    /// @param userReserveA - user reserve in token A
    ///@param userReserveB - user reserve in token B
    /// @return liquidity - user LP token amount
    function _calculateUserLP(
        uint256 userReserveA,
        uint256 userReserveB
    ) private view returns (uint256 liquidity) {
        uint256 totalLP = IUniswapV2ERC20(pair).totalSupply();
        if (userReserveA == 0 || userReserveB == 0) return (0);

        (uint256 reserveA, uint256 reserveB, ) = IUniswapV2Pair(pair).getReserves();

        // unuswap pair liquidity
        liquidity = _min(
            (userReserveA * totalLP) / reserveA,
            (userReserveB * totalLP) / reserveB
        );
    }

    /// @dev Calculates Min of two numbers
    /// @param a - value a
    /// @param b - value b
    /// @return min - min of two values
    function _min(uint256 a, uint256 b) private pure returns (uint256 min) {
        min = (a < b) ? a : b;
    }

    /// @dev Substracts b from a without underflow
    /// @param a - value a
    /// @param b - value b
    /// @return res - a - b
    function _safeDecrease(uint256 a, uint256 b) private pure returns (uint256 res) {
        res = (a > b) ? a - b : 0;
    }

    /// @dev Allows to view staking amount of selected user
    /// @param account to view balance
    /// @return balance of staking tokens for selected account
    function balanceOf(address account) external view returns (uint256) {
        return userBalances[account];
    }

    /// @dev Allows to view user stake ids
    /// @param user user account
    /// @return array of user ids
    function getUserStakeIds(address user) external view returns (uint256[] memory) {
        return idsByUser[user].values();
    }

    /// @dev Allows to view user`s stake ids quantity
    /// @param user user account
    /// @return length of user ids array
    function getUserStakeIdsLength(address user) external view returns (uint256) {
        return idsByUser[user].values().length;
    }

    /// @dev Allows to get a slice user stakes array
    /// @param user user account
    /// @param startIndex Starting index in user ids array
    /// @param length return array length
    /// @return Array-slice of user stakes ids
    function getUserStakesIdsSlice(
        address user,
        uint256 startIndex,
        uint256 length
    ) external view returns (Stake[] memory) {
        uint256[] memory ids = idsByUser[user].values();
        uint256 len = ids.length;
        require(startIndex + length <= len, 'Invalid startIndex + length');

        Stake[] memory userStakes = new Stake[](length);
        uint256 userIndex;
        for (uint256 i = startIndex; i < startIndex + length; ++i) {
            uint256 stakeId = ids[i];
            userStakes[userIndex] = stakes[stakeId];
            ++userIndex;
        }

        return userStakes;
    }

    /// @dev Sets paused state for the contract (can be called by the owner only)
    /// @param paused paused flag
    function setPaused(bool paused) external onlyOwner {
        if (paused) {
            _pause();
        } else {
            _unpause();
        }
    }

    /// @dev Returns maximum potential reserves for a stake with given id
    /// @param maxReserveA, maxReserveB
    function getStakeMaxReserves(
        uint256 id
    ) external view returns (uint256 maxReserveA, uint256 maxReserveB) {
        return _calculateMaxPotentialReserves(stakes[id].reserves);
    }

    /// @dev Allows to get a slice user stakes history array
    /// @param user user account
    /// @param startIndex Starting index in user ids array
    /// @param length return array length
    /// @return Array-slice of user stakes history
    function getUserInactiveStakesSlice(
        address user,
        uint256 startIndex,
        uint256 length
    ) external view returns (Stake[] memory) {
        uint256 len = userInactiveStakes[user].length;
        require(startIndex + length <= len, 'Invalid startIndex + length');
        Stake[] memory userStakes = new Stake[](length);
        uint256[] memory userInactiveStakes_ = userInactiveStakes[user];
        uint256 userIndex;
        for (uint256 i = startIndex; i < startIndex + length; ++i) {
            uint256 stakeId = userInactiveStakes_[i];
            userStakes[userIndex] = stakes[stakeId];
            ++userIndex;
        }
        return userStakes;
    }

    /// @dev Allows to view user`s closed stakes quantity
    /// @param user user account
    /// @return length of user closed stakes array
    function getUserInactiveStakesLength(address user) external view returns (uint256) {
        return userInactiveStakes[user].length;
    }

    /// @dev Allows to view the number of stake holders
    /// @return length of stake holders array
    function getHoldersLength() external view returns (uint256) {
        return holders.length();
    }

    /// @dev Allows to get a slice of stake holders array
    /// @param startIndex Starting index in stake holders array
    /// @param length return array length
    /// @return Array-slice of stake holders
    function getHoldersSlice(
        uint256 startIndex,
        uint256 length
    ) external view returns (address[] memory) {
        require(startIndex + length <= holders.length(), 'Invalid startIndex + length');
        address[] memory holdersArray = new address[](length);
        for (uint256 i = 0; i < length; ) {
            holdersArray[i] = holders.at(i);
            unchecked {
                ++i;
            }
        }
        return holdersArray;
    }
}
